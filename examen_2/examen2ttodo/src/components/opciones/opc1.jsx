
import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';

export default function Opc1(){
  
    const [idj, setId] = useState([]);
    const url ='https://jsonplaceholder.typicode.com/todos'
  
      const showData= async()=>{
        const response = await fetch(url)
        const data = await response.json()
        setId(data)
      }
      useEffect( ()=>{
        showData()
      },[])
  
    const columns = [
      
      {
        name: 'Id',
        selector: row =>row.id,
      },
  
    ];
  
    return (
      <DataTable
      columns={columns}
      data={idj}
      pagination
      />
        
    );
  };

