
import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';

export default function Opc6(){
  
    const [idUsj, setIdUs] = useState([]);
    const url ='https://jsonplaceholder.typicode.com/todos'
  
      const showData= async()=>{
        const response = await fetch(url)
        const data = await response.json()
        setIdUs(data)
      }
      useEffect( ()=>{
        showData()
      },[])
    
    const filtrador = idUsj.filter(i=> i.completed === true)
    const columns = [
      
      {
        name: 'Id',
        selector: row =>row.id,
      },
      {
        name: 'Usuario',
        selector: row =>row.userId,
      },
  
    ];
  
    return (
      <DataTable
      columns={columns}
      data={filtrador}
      pagination
      />
        
    );
  };

