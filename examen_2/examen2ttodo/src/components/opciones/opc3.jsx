
import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';

export default function Opc3(){
  
    const [datosjson, setdatos] = useState([]);
    const url ='https://jsonplaceholder.typicode.com/todos'
  
      const showData= async()=>{
        const response = await fetch(url)
        const data = await response.json()
        setdatos(data)
      }
      useEffect( ()=>{
        showData()
      },[])
    const datosjsonFiltrador= datosjson.filter(i => i.completed === false);
    const columns = [
      
      {
        name: 'Id',
        selector: row =>row.id,
      },
      {
        name: 'Titulo',
        selector: row =>row.title,
      },
  
    ];

  
    return (
      <DataTable
      columns={columns}
      data={datosjsonFiltrador}
      pagination
      />
        
    );
  };

