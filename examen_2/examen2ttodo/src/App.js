import './App.css';
import React, { useState } from 'react';


import Cabesera from './components/cabesera';

import Opc1 from './components/opciones/opc1';
import Opc2 from './components/opciones/opc2';
import Opc3 from './components/opciones/opc3';
import Opc4 from './components/opciones/opc4';
import Opc5 from './components/opciones/opc5';
import Opc6 from './components/opciones/opc6';
import Opc7 from './components/opciones/opc7';
function App() {
const [opcion, setop] = useState('0');
const decision = (a)=>{
  switch(a){
    case '1': return <Opc1/>
    case '2': return <Opc2/>
    case '3': return <Opc3/>
    case '4': return <Opc4/>
    case '5': return <Opc5/>
    case '6': return <Opc6/>
    case '7': return <Opc7/>
    default: return <h1>seleccione la opcion en filtro</h1>
  }
}

  return (
    <div>
      <Cabesera/>
      <br/>
      

      <div class="text-center ">
        <div class="row">
          <div class="col-3" style={{ 'padding-left': 40 }}>
          <div class="list-group">
            <button type="button" class="list-group-item list-group-item-action active" disabled  aria-current="true">
            Fltrar
            </button>
            <button type="button" class="list-group-item list-group-item-action" onClick={() => setop('1')}> Solo ID</button>
            <button type="button" class="list-group-item list-group-item-action" onClick={() => setop('2')}>ID y titulos</button>
            <button type="button" class="list-group-item list-group-item-action" onClick={() => setop('3')}>Sin resolver ID y titulos</button>
            <button type="button" class="list-group-item list-group-item-action" onClick={() => setop('4')}>Completado ID y titulos</button>
            <button type="button" class="list-group-item list-group-item-action" onClick={() => setop('5')}>ID y usersID</button>
            <button type="button" class="list-group-item list-group-item-action" onClick={() => setop('6')}>Completados ID y usersID</button>
            <button type="button" class="list-group-item list-group-item-action" onClick={() => setop('7')}>Sin resolver ID y usersID</button>
          </div>
          </div>
          <div class="col-9"> 
            {decision(opcion)}
          
          
          
          </div>
        </div>
      </div>
    </div>
    
  );
}

export default App;
