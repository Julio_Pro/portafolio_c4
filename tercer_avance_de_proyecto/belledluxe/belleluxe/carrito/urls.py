from django.urls import path

from . import views

app_name = 'carrito'
 
urlpatterns = [
    path('', views.corder, name="order"),
    path('add/',views.carrito_add, name ="carrito_add"),
    # path('detail/carrito/<int:pk>/', views.DetailCarrito, name='detail_carrito'),
    path('update/', views.UpdateCarrito, name='update_carrito'),
    path('delete/', views.DeleteCarrito, name='delete_carrito'),
    path('pago/', views.pagoos.as_view(), name='pagoC'),
    path('pedidos/', views.verPedidos.as_view(), name='pedidos'),


]