from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from core.models import *


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom claims
        token['username'] = user.username
        #token['email'] = user.email
        # ...

        return token



class seriCustomer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'

class sericard(serializers.ModelSerializer):
    class Meta:
        model = Cart
        fields = ['product', 'quantity']


class SeriCampaign(serializers.ModelSerializer):
    class Meta:
        model = campaign
        fields = '__all__'

class SeriCategory(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

class SeriProduct(serializers.ModelSerializer):
    # imagen1 = serializers.ImageField(required=True)
    class Meta:
        model = Product
        fields = '__all__'
        
class SeriItemPedido(serializers.ModelSerializer):
    class Meta:
        model = ItemPedido
        fields = '__all__'

class SeriPedido(serializers.ModelSerializer):
    class Meta:
        model = Pedido
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'









        