import NavTienda from "../components/tienda/NavTienda";
import ToolTienda from "../components/tienda/ToolTienda";
import FooterTienda from "../components/tienda/FooterTienda";
import '../css/tienda/styletienda.css'
import axios from "../utils/axios";
import React from 'react';
import { useEffect, useState } from 'react';

function Product(props) {
  const { id, name, price, imagen1 } = props.product;

  return (
    <div className="product-box" style={{ 'color':'#ffffff'}}>
      <a href={`/producto/${id}`} className="product-link">
        <img src={imagen1} alt={name} className="product-img" />
        <h2 className="product-title">{name}</h2>
        <span className="price">{price}$</span>
        <i className='bx bx-shopping-bag add-cart'></i>
      </a>
    </div>
  );
}

function Shop() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    axios.get('tienda/')
      .then(response => {
        setProducts(response.data);
      })
      .catch(error => {
        console.error('Error fetching products:', error);
      });
  }, []);

  return (
    <>

<body style={{ border: "1px solid white"}}>
<NavTienda/>
<ToolTienda/>
<section className="shop container">
      <div className="shop-content">
        {products.map(product => (
          <Product key={product.id} product={product} />
        ))}
      </div>
    </section>
<FooterTienda/>

</body>
</>
    
  );
}

export default Shop;
