import NavCart from '../components/carrito/NavCart';
import axios from '../utils/axios';
import { useEffect, useState } from 'react';
import Cookies from 'js-cookie';
import { BorrarProd } from '../utils/carrito';
import TextCart from '../components/carrito/TextCart';
import { TrashIcon } from '@heroicons/react/outline';
import {  Button } from "@material-tailwind/react";

export default function Carrito() {
    const [cartData, setCartData] = useState(null);
    const [error, setError] = useState(null);
    const [quantity, setq]= useState(null);

    useEffect(() => {
        const fetchCartData = async () => {
        try{
                const accessToken = Cookies.get('access_token');
                const response = await axios.get('cart/',{
                    headers: {
                      'Authorization': `Bearer ${accessToken}`
                    }
                });
                setCartData(response.data)
            }catch (error) {
               
                setError(error.response.data.error);
            }
        };
        fetchCartData()
    }, []);

    
    const actualizarCantidad = (index, event) => {
        const newQuantity = parseInt(event.target.value);
        const updatedCartData = { ...cartData };
        updatedCartData.cantidad[index].quantity = newQuantity;
        setCartData(updatedCartData);
    };
    const calcularCantidad = (index) => {
        if (cartData && cartData.cantidad && cartData.carrito && cartData.cantidad[index] && cartData.carrito[index]) {
            const cantidadCarrito = cartData.cantidad[index].quantity;
            const cantidadDisponible = cartData.carrito[index].stock;

            return cantidadCarrito > cantidadDisponible ? cantidadDisponible : cantidadCarrito;
        }
        return 0;
    };
    const handlepedido = async () => {
        try {
            const productos = cartData.carrito.map((producto) => ({
                id: producto.id,
                cantidad: calcularCantidad(cartData.carrito.indexOf(producto))
            }));
            const data = { productos };
            console.log(data)

            const accessToken = Cookies.get('access_token');
            const response = await axios.post('pedido/add', data, {
                headers: {
                    'Authorization': `Bearer ${accessToken}`
                  }
            });

            console.log('Respuesta del servidor:', response.data);
            setCartData(null);
        } catch (error) {
            console.error('Error al procesar el pago:', error);
            setError(error.response.data.error);
        }
    };

    const handleBorrarProd = async (productId) => {
        try {
            await BorrarProd(productId);
            const updatedCartData = { ...cartData };
            updatedCartData.carrito = updatedCartData.carrito.filter(producto => producto.id !== productId);
            setCartData(updatedCartData);
        }catch (error) {
            setError(error);
        }
    };
    const calcularPrecioTotalProducto = (producto) => {
        return producto.price * calcularCantidad(cartData.carrito.indexOf(producto));
    };

    const calcularPrecioTotalCarrito = () => {
        let total = 0;
        cartData.carrito.forEach((producto) => {
            total += calcularPrecioTotalProducto(producto);
        });
        return total;
    };
    if (error) {
        return <div>Error: {error}</div>;
      }
    
    if (!cartData) {
        return <div>Loading...</div>;
    }
    return (

        <body>
            
       <div>
        <NavCart />

        </div>
        <div style={{'color':'white'}}>
           <TextCart/>

           </div>
           <table className='text-white border '>
           <tr className='border'>
        <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6"><strong>Imagen</strong></td>
        <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6"><strong>Nombre del Producto</strong></td>
        <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6"><strong>Talla</strong></td>
        <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6"><strong>Cantidad</strong></td>
        <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6"><strong>Precio</strong></td>
        <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6"></td> {/* Espacio para el botón */}
    </tr>
    </table>
            <ul>
                {cartData.carrito.map((producto, index) => (
                    <li key={index}>



    
<table className='text-white border '>
    
    <tr>
        <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6"><img src={`http://127.0.0.1:8000${producto.imagen1}`} style={{ maxWidth: '200px' }} /></td>
        <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6">{producto.name}</td>
        <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6">{producto.size}</td>
        <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6">
            <select style={{'color':'black'}} value={calcularCantidad(index)} onChange={(event) => actualizarCantidad(index, event)}>
                {[...Array(producto.stock).keys()].map((cantidad) => (
                    <option key={cantidad + 1} value={cantidad + 1}>{cantidad + 1}</option>
                ))}
            </select>
        </td>
        <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6">${calcularPrecioTotalProducto(producto).toFixed(2)}</td>
        <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6 text-right">
            <button onClick={() => handleBorrarProd(producto.id)} className="bg-transparent border-none">
                <TrashIcon className="h-6 w-6 text-white" />
            </button>
        </td>
        
        
    </tr>
</table>
                
                    
                    </li>
                ))}
            </ul>
            <table className='text-white border '>
                <tr className='border'>
                    <td className="
                    ">
                        <Button className=" bg-transparent border border-white text-white rounded-none"  onClick={handlepedido} style={{ margin: 3 }} >
                        <strong> pagar</strong>
                        </Button>
                        </td>
                    <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6"><strong></strong></td>

                    <td className="px-2 py-1 sm:px-4 sm:py-2 md:px-6"><strong>Total del carrito: ${calcularPrecioTotalCarrito().toFixed(2)}</strong></td>
            
                </tr>
              
          
        </table>
           
        </body>
 
    );
  }

