import React from "react";
import ReCustomer from "../../../components/ApiCruds/customer/ReCustomer";
import AdminSlidebar from "../../../components/AdminSlidebar";

export default function Customer() {
    return (
        <div className="min-h-screen flex">
        <AdminSlidebar />
        <div className="flex-1 p-6">
        <ReCustomer/>
        </div>
      </div>
    );
}