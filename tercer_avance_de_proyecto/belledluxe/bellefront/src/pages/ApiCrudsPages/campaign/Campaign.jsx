import React from "react";
import ReCampaign from "../../../components/ApiCruds/campaign/ReCampaign";
import AdminSlidebar from "../../../components/AdminSlidebar";

export default function Campaign() {
    return (
      <div className="flex">
        <AdminSlidebar />
        <div className="w-3/4 p-6">
          <ReCampaign />
        </div>
      </div>
    );
}
