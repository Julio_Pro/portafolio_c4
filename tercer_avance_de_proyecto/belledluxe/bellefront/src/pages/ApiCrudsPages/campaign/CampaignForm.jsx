import React, { useEffect, useState } from "react";
import { useForm } from 'react-hook-form';
import { createCampaign, DeleteCampaign, UpdateCampaign, getCampaign } from "../../../api/Campaign_api";
import { getAllUsers } from "../../../api/Users";
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-hot-toast';
import NavApi from "../../../components/ApiCruds/NavApi";
import FooterApi from "../../../components/ApiCruds/FooterApi";

export default function CampaignForm() {
    const [users, setUsers] = useState([]);
    const [selectedUser, setSelectedUser] = useState(null);
    const { register, handleSubmit, formState: { errors }, setValue } = useForm();
    const navigate = useNavigate();
    const params = useParams();
    const handleClick = () => {
        navigate('../ReCampaigns/');
    };
 
    useEffect(() => {
        async function loadData() {
            const usersResponse = await getAllUsers();
            setUsers(usersResponse.data);
            if (params.id) {
                const { data } = await getCampaign(params.id);
                setSelectedUser(data.lasteditor);
                setValue('name', data.name, { shouldFocus: true });
            }
        }
        loadData();
    }, [params.id, setValue]);

    const onSubmit = handleSubmit(async data => {
        if (params.id) {
            await UpdateCampaign(params.id, { ...data, lasteditor: selectedUser });
            toast.success('Actualización realizada con exito');

        } else {
            await createCampaign({ ...data, lasteditor: selectedUser });
            toast.success('Campaña creada exitosamente');
        }
        navigate('../ReCampaigns');
    });

    return (
        <div>
            <NavApi/>
        
        <div className="container mx-auto mt-5 text-white">
            <br />
            <br />
            <form onSubmit={onSubmit} className="max-w-md mx-auto">
                <label htmlFor="lasteditor" className="block mb-2">Último Editor:</label>
                <select id="lasteditor" {...register("lasteditor", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white" value={selectedUser} onChange={e => setSelectedUser(e.target.value)}>
                    {users.filter(user => user.is_superuser).map(user => (
                        <option key={user.id} value={user.id}>{user.username}</option>
                    ))}
                </select>
                {errors.lasteditor && <span className="text-red-500">Este campo es requerido.</span>}

                <label htmlFor="name" className="block mb-2">Nombre:</label>
                <input type="text" id="name" {...register("name", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500" placeholder="Nombre" />
                {errors.name && <span className="text-red-500">Este campo es requerido.</span>}

                <br />
                
                <button type="submit" className="block w-full px-4 py-2 bg-blue-500 rounded hover:bg-blue-600 focus:outline-none focus:bg-blue-600">Guardar</button>

                {params.id &&
                <button onClick={async () => {
                    const accepted = window.confirm("¿Estás seguro de que deseas eliminar esta campaña definitivamente?");if (accepted) {
                        await DeleteCampaign(params.id);
                        toast.success('Campaña eliminada exitosamente');
                        navigate('../ReCampaigns');
                    }
                }} className="block w-full px-4 py-2 mt-4 bg-red-500 rounded hover:bg-red-600 focus:outline-none focus:bg-red-600">Eliminar</button>}

                    <br />
                    <hr />
                    <br />
                    <button onClick={handleClick} className="block w-full px-4 py-2 bg-gray-500 rounded hover:bg-gray-600 focus:outline-none focus:bg-gray-600">Cancelar</button>

            </form>
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />

        <FooterApi />
    </div>
    );
}