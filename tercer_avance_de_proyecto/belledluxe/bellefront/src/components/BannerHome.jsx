import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import BannerOne from '../assets/images/BannerOne.png';
import Video from '../assets/videos/VideoBanner.mp4'; 
import BannerTwo from '../assets/images/BannerTwo.png';


const BannerHome = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 800,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnHover: true,
  };
 
  const images = [
    { type: 'image', src: BannerOne, alt: "Imagen 1" }, // Primer elemento como imagen
    { type: 'video', src: Video, alt: "Video 1" },   // Segundo elemento como video
    { type: 'image', src: BannerTwo, alt: "Imagen 3" }
  ];

  return (
    <div style={{ margin: '0 auto', maxWidth: '900px', marginTop: '20px', marginBottom: '20px', paddingTop : 24 }}>

      <Slider style={{ margin: '2rem'}} {...settings}>
        {images.map((media, index) => (
          <div key={index}>
            {media.type === 'image' ? (
              <img src={media.src} alt={media.alt} />
            ) : (
              <video autoPlay loop muted controls style={{ width: '100%', height: 'auto' }}>
                <source src={media.src} type="video/mp4" />
                Your browser does not support the video tag.
              </video>
            )}
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default BannerHome;
