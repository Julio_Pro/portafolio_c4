import React from "react";
import logobdl from "../../../assets/images/logobdl.png";

export default function NavLog() {
  return (
    <>

      <nav className="p-6 flex justify-center items-center" style={{ borderBottom: "1px solid white" }}>

      <div className="flex items-center">
        <a href="/">
          <img
            src={logobdl}
            alt="logo"
            style={{ width: "250px", height: "auto" }}
          />
        </a>
      </div>
    </nav>
    </>
  );
}
