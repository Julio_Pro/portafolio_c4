import React, { useEffect, useState } from 'react';
import {login} from '../../../utils/auth'
import { useNavigate } from 'react-router-dom'
import { useAuthStore } from '../../../store/auth'

const LoginForm = () => {
  const navigate = useNavigate()
  const [username, setusername] = useState('');
  const [password, setPassword] = useState('');

  const isLoggedIn = useAuthStore((state) => state.isLoggedIn)

  useEffect(()=>{
    if(isLoggedIn()){
      navigate('/')
    }
  },[])
  const resetForm= ()=>{
    setPassword('');
    setusername('')
  }
  const handleLogin = async (e)=> {
    e.preventDefault();
    const {error}= await login(username, password)
    if (error){
      alert(error)
    }else{
      navigate('/')
      resetForm()
    }
   
  };

  return (
    <div className="flex justify-center items-center h-screen">
      <form onSubmit={handleLogin} className="border shadow-md rounded-none px-12 pt-12 pb-8 mb-4">
        <div className="mb-4">
          <label className="block text-white text-sm font-bold mb-2" htmlFor="email">
            Username
          </label>
          <input
            className="shadow appearance-none  border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="username"
            type="username"
            placeholder="username"
            value={username}
            maxLength={15}
            onChange={(e) => setusername(e.target.value)}
          />
        </div>
        <div className="mb-6">
          <label className="block text-white text-sm font-bold mb-2" htmlFor="password">
            Password
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            id="password"
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className="flex items-center justify-between">
          <button
            className="bg-white text-black font-bold py-1 px-3 rounded-none focus:outline-none focus:shadow-outline"
            type="submit"
            style={{fontSize : "12px"}}
          >
            Iniciar Sesion
          </button>
     
          <a href="/Register"
            className="text-white font-bold py-1 px-3 rounded-none focus:outline-none focus:shadow-outline"
            style={{fontSize : "12px"}}>
            Registrarse
          </a>
      
 
        </div>
      </form>
    </div>
  );
};

export default LoginForm;
