import React from "react";
import {
  Accordion,
  AccordionHeader,
  AccordionBody,
} from "@material-tailwind/react";

import imgForma from "../../assets/images/imgForma.png";

export default function AccordionA() {
  const [open, setOpen] = React.useState(1);

  const handleOpen = (value) => setOpen(open === value ? 0 : value);

  return (
    <>
      <p className="text-white p-12 text-1xl text-justify">
        La moda contribuye a los elementos que se relacionan de manera que
        muesren expresion, comunicacion e identidad. Los colores son la parte
        que suma a la relevancia en la vestimenta es una forma de transmitir
        emociones,, sensaciones y mensajes. Tambien tienen un significado
        simbolico y cultural, que puede variar segun el contexto y la
        interpretacion, que se asocia al estilo personal, que se dapata a las
        preferencias, a la personalidad, y las necesidades de cada persona.
        Nosotros queremos ayudar a que nuestros consumidores conozcan su propio
        estilo y forma, de acuerdo a sus cualidades fisicas.
      </p>
      <br />

      <Accordion
        open={open === 1}
        className="mb-2 border border-white px-4"
      >
        <AccordionHeader
          onClick={() => handleOpen(1)}
          className={`border-b-0 transition-colors ${
            open === 1 ? "text-white hover:!text-white" : ""
          }`}
          style={{ fontFamily: "Times New Roman, serif"}}
        >
          Tipo de Forma
        </AccordionHeader>
        <AccordionBody className="pt-0 text-base text-white text-1xl" style={{ fontFamily: "Times New Roman, serif"}}>
          <br />
          Si su cuerpo tiene forma de rectangulo . Opta por vestidos con
          cinturones, frunces, pliegues o volantes.
          <br /> <br />
          Si su cuerpo tiene forma de triangulo invertido. Opta por vestidos con
          vuelo, envase acampanados, o con estampados llamativos.
          <br /> <br />
          Si su cuerpo tiene forma de reloj de arena Opta por vestidos
          ajustados, entallados, cruzados o con escotes en V.
          <br /> <br />
          <img src={imgForma} alt="Formas de cuerpo" style={{ maxWidth: '30%', height: 'auto' }} />
         <hr />
        </AccordionBody>
      </Accordion>
      <Accordion
        open={open === 2}
        className="mb-2 border border-white px-4"
      >
        <AccordionHeader
          onClick={() => handleOpen(2)}
          className={`border-b-0 transition-colors ${
            open === 2 ? "text-white hover:!text-white" : ""
          }`}
          style={{ fontFamily: "Times New Roman, serif"}}
        >
          Colores de vestimenta.
        </AccordionHeader>
        <AccordionBody className="pt-0 text-base text-white text-1xl"   style={{ fontFamily: "Times New Roman, serif"}}>
          Los colores oscuros como el negro, el azul marino o el burdeos son
          elegantes y favorecedores
          <br />
          <br />
          Los colores brillantes como el rojo, el fucsia o el dorado, son
          llamativos y divertidos
          <br />
          <br />
          Los zapatos pueden tener brillo lentejuelas o cristales para darles un
          toque glamuroso a tu look
          <br />
          <br />
          <hr />
        </AccordionBody>
      </Accordion>
    </>
  );
}
