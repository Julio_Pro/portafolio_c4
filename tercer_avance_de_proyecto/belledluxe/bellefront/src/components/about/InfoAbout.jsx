import React from "react";

export default function InfoAbout() {
  return (
    <div>
      <p className="text-white text-base" style={{fontSize : "20px", textAlign:"justify", marginBottom: "8%" }}>
        Nosotros en BELLE D’LUXE, somos una marca de moda de lujo para hombres y
        mujeres que se originó como parte de Rubis Collection, fundada por un
        equipo comprometido y talentoso: Xahil Alvarez Márquez, Julio Barrón
        Quintana, Karime Pulido Limón e Itzel Ruiz Pacheco. Nuestros valores
        fundamentales incluyen el respeto, la responsabilidad, la igualdad, el
        compromiso, la calidad y empatía, así como la libertad. Respetamos a
        cada individuo en todos los aspectos, asumimos responsabilidad en todas
        nuestras acciones, promovemos la igualdad y la diversidad, nos
        comprometemos a ofrecer lo mejor en un ambiente positivo, valoramos la
        calidad y empatía en nuestro servicio al cliente, y permitimos la
        libertad creativa de nuestro equipo, siempre con respeto hacia los demás
        y nuestros objetivos laborales.
      </p>
    </div>
  );
}


