import React, { useRef } from "react";
import '../../css/contact/formulario.css'
import emailjs from '@emailjs/browser';
import BoxAbout from "../about/BoxAbout";


export const Prueba2 = () =>  {

    const refForm = useRef (); 
    const handleSubmit = (event) => {
    event.preventDefault();
    const serviceId = "service_478wgby"; 
    const templateId = "template_tzowsme"; 
    // 3erparametro
    const apikey = "IR47g6rwQ8mOaAvol";


    emailjs.sendForm(serviceId, templateId, refForm.current, apikey)
    .then( result => console.log(result.text))
    .catch( error => console.error(error))
}

return ( 
    <form ref={refForm} action="" onSubmit={handleSubmit} className="border p-8 text-center m-5 border rounded text-xl">
        <div className="header-contact text-white font-bold" >
            <h2>FORMULARIO</h2>
        </div>
        <fieldset className="field-name">
            <label className="symbol-required name p-10" htmlFor="">Nombre</label>
            <input name="username" className="bg-transparent border" type="text" placeholder="Ej: Ruiz Monica" required />
        </fieldset>
        <fieldset className="field-email">
            <label className="symbol-required p-2" name= "email">Correo electrónico</label>
            <input placeholder="Ej: moniruiz@email.com" className="bg-transparent border" type="email" name="email" id="email" required/> 
        </fieldset>
        <fieldset className="field-message">
            <label className="symbol-required p-11" name= "email">Mensaje o comentario</label>
            <textarea maxLength="500" name="message" className="bg-transparent border rounded" placeholder="Escribe tu mensaje." id="" cols="30" rows="10"></textarea>
        </fieldset>
        <button className='btn-send text-white border p-3'>Enviar</button>
    </form>
    )
    
}

