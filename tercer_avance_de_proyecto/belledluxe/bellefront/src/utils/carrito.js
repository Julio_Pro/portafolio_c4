import Cookies from 'js-cookie';
import axios from './axios';


export const agCarrito = async(n1,n2)=>{
    const accessToken = Cookies.get('access_token');
    try{
        const { data } = await axios.post('cart/add/',{
            "product_id": n1,
            "quantity": n2
          }
          ,{
            headers: {
              'Authorization': `Bearer ${accessToken}`
            }
          });
        return { data, error: null };
    } catch (error) {
        return {
            data: null,
            error: error || 'Algo malo paso',
        };
    }
};

export const BorrarProd = async(n1)=>{
  const accessToken = Cookies.get('access_token');
  try{
      const { data } = await axios.post('cart/borrar/',{
          "product_id": n1
        }
        ,{
          headers: {
            'Authorization': `Bearer ${accessToken}`
          }
        });
      return { data, error: null };
  } catch (error) {
      return {
          data: null,
          error: error || 'Algo malo paso',
      };
  }
};

