import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import Rutas from './rutas';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';



const queryClient = new QueryClient();



ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <Rutas />
    </QueryClientProvider>
  </React.StrictMode>,
)
