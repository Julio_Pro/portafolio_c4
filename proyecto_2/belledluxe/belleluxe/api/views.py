from django.shortcuts import render
from rest_framework import generics, viewsets, permissions
from rest_framework.permissions import  IsAdminUser, AllowAny
from core.models import  *
from .serializers import *
from rest_framework.views import APIView
"""from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token"""

from rest_framework.response import Response
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_protect
from django.utils.decorators import method_decorator
from django.contrib.auth.models import User
from rest_framework import permissions

from django.contrib import auth
from django.utils.crypto import get_random_string

# Create your views here.

# Revisar autenticacion de los usuarios
@method_decorator(csrf_protect, name='dispatch')
class CheckAuthenticatedView(APIView):
    def get(self, request, format = None):

        try:

            isAuthenticated = User.is_authenticated

            if isAuthenticated:
                return Response({'isAuthenticated' : 'success'})
            else:
                return Response({'isAuthenticated' : 'error'})
        except:
            return Response({'error': 'Algo salio mal'})



# Cruds
class CrudCampaign(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    serializer_class = SeriCampaign
    queryset = campaign.objects.all()

class CrudCategory(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    serializer_class = SeriCategory
    queryset = Category.objects.all()

class CrudCustomer(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    serializer_class = seriCustomer
    queryset = Customer.objects.all()

class CrudProductos(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    serializer_class = SeriProduct
    queryset = Product.objects.all()

class CrudItemPedido(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    serializer_class = SeriItemPedido
    queryset = ItemPedido.objects.all()

class CrudPedido(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    serializer_class = SeriPedido
    queryset = Pedido.objects.all()


    

# Tienda
class ListaProductos(generics.ListAPIView):
    serializer_class = SeriProduct
    queryset = Product.objects.filter(status=True)

# Sesion
@method_decorator(csrf_protect, name='dispatch')
class RegistroAPI(APIView):
    permission_classes = [AllowAny]

    def post(self, request, format=None):
        data =  self.request.data


        username = data['username']
        password = data['password']
        re_password = data['re_password']

        try:
            if password == re_password:
                    if User.objects.filter(username=username).exists():
                        return Response({'error':'El usuario ya existe'})
                    else:
                        if len(password)<6:
                            return Response({'error':'La contrasena es corta'})
                        else:
                            user = User.objects.create_user(username=username, password=password)
                            user.save()

                            user = User.objects.get(id=user.id)

                            user_cus = Customer(user=user, name_first='', last_name_pat='', last_name_mat='', phone='', cp='', streat='', number_home='')
                            user_cus.save()

                            return Response({'success': 'se creo el usuario'})
            else:
                return Response({'error':'No concuerdan las contra'})
        except:
            return Response({'error': 'Algo salio mal mientras registrabas tu cuenta'})



@method_decorator(ensure_csrf_cookie, name='dispatch')
class getcsrftoken(APIView):
    permission_classes = [AllowAny]
    def get(self, request, format=None):
        return Response({'success':'CSRF cookies set'})
    


# Iniciar sesion 
@method_decorator(csrf_protect, name='dispatch')
class LoginView(APIView):
    permission_classes = [AllowAny]

    def post(self, request, format=None):
        data =  self.request.data

        username = data['username']
        password = data['password']

        try:
            user = auth.authenticate(username=username, password=password)

            if user is not None:
                auth.login(request, user)
                return Response({'success': 'Se autentico usuario', 'username': username})
            else:
                return Response({'error': 'Ocurrio un error en la autenticación'})

        except:
            return Response({'error': 'Algo sallio mal'})




#Cerrar sesion 

class LogoutView(APIView):
    def post(self, request, format=None):
        
        try:
            auth.logout(request)
            return Response({'success': 'Se cerro sesión correctamente'})
        except:
            return Response({'error': 'Algo salio mal al intentar cerrar sesión'})
        


    
#Borrar una cuentaa
class DeleteAccountView(APIView):
    def delete(self, request, format=None):
        
        user = self.request.user
        try:
            user = User.objects.filter(id=user.id).delete()
            return Response({'success': 'Usuario eliminado'})
        except:
            return Response({'error': 'Algo salio mal al intentar eliminar el usuario'})



#Usuarios 
class GetUsersView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, format=None):
        users = User.objects.all()

        users = UserSerializer(users, many=True)
        return Response(users.data)
    

#Perfil

class GetCustomerView(APIView):
    def get(self, request, format=None):
        try: 
            user = self.request.user

            user_obj = User.objects.get(id=user.id)
      

            customer_profile = Customer.objects.get(user=user.id)
    

            serializer = seriCustomer(customer_profile)

            return Response({'profile': serializer.data, 'username': str(user.username)})
        except User.DoesNotExist:
            return Response({'error': 'El usuario no existe.'})
        except Customer.DoesNotExist:
            return Response({'error': 'El perfil de cliente no existe.'})
        except Exception as e:
            return Response({'error': 'Algo salió mal al intentar recuperar el perfil. Detalle: {}'.format(str(e))})


#Actualizar perfil

class UpdateCustomerProfileView(APIView):
    def post(self, request, format=None):
            try:
        
                user = self.request.user
                username = Customer.user

                data = self.request.data
                user = data['user']
                name_first= data['name_first']
                last_name_pat= data['last_name_pat']
                last_name_mat= data['last_name_mat']
                phone= data['phone']
                cp= data['cp']
                streat= data['streat']
                number_home= data['number_home']


                user= User.objects.get(id=user.id)

                Customer.objects.filter(user=user).update(name_first=name_first, last_name_mat=last_name_mat, last_name_pat=last_name_pat,phone=phone,cp=cp, streat=streat, number_home=number_home)

                customer_pro = Customer.objects.get(user=user)

                return Response({'profile': customer_pro.data, 'username': str(username) })
            
            except:
                return Response({'error': 'Algo salio mal al intentar actualizar perfil'})


class verpedidoAPI(APIView):
    def get(self, request, format=None):
        try:
            user = self.request.user
            
            user_ob = User.objects.get(id=user.id)
            pedidos_dl = Pedido.objects.filter(usuario=user.id)
            serializer = SeriPedido(pedidos_dl, many=True)
            
            return Response(serializer.data)
        except Pedido.DoesNotExist:
            return Response({'error': 'No hay no existe .'})
        except Exception as e:
            return Response({'error': 'Error al intentar recuperar pedido. Detalle: {}'.format(str(e))})


class pedidoAPI(APIView):
    def post(self, request, format=None):
        try:
            user= self.user.user
            pedido= Pedido.objects.create(usuario=request.user, codigo_pedido=get_random_string(length=8))
            datos = request.data.get('productos',[])
            for datos in datos:
                pedido = datos.get('pedido')
                producto = datos.get('product')
                cantidad = datos.get('cantidad')
                ItemPedido.objects.create(pedido=pedido, product=producto, cantidad=cantidad, codigo_producto= None)
        except:
            return Response({'error':'productos agotados'})








