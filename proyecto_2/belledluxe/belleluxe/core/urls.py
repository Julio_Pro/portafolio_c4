from django.contrib import admin
from django.urls import path, include
from core import views


app_name="core"
urlpatterns=[
    path('', views.adm.as_view(), name="adm"),

    path('camping/', views.Campaign.as_view(), name="campaign"),
    path('detail/camping/<int:pk>/', views.DetailCampaign.as_view(), name='detail_campaign'),
    path('update/camping/<int:pk>/', views.UpdateCampaign.as_view(), name='update_campaign'),
    path('delete/camping/<int:pk>/', views.DeleteCampaign.as_view(), name='delete_campaign'),
    path('new/camping/', views.NewCampaign.as_view(), name='Newcampaign'),

    path('list/category/', views.list_category.as_view(), name="category"),
    path('detail/category/<int:pk>/', views.DetailCategory.as_view(), name='detail_category'),
    path('update/category/<int:pk>/', views.UpdateCategory.as_view(), name='update_category'),
    path('delete/category/<int:pk>/', views.DeleteCategory.as_view(), name='delete_category'),
    path('new/category/', views.NewCategory.as_view(), name='Newcategory'),

    path('product/', views.product.as_view(), name="product"),
    path('recover/product/<int:pk>/', views.RecoverProduct.as_view(), name='recover_product'),
    path('update/product/<int:pk>/', views.UpdateProduct.as_view(), name='update_product'),
    path('delete/product/<int:pk>/', views.DeleteProduct.as_view(), name='delete_product'),
    path('new/product/', views.NewProduct.as_view(), name='new_product'),

    path('list/customer/', views.list_customer.as_view(), name="customer"),
    path('detail/customer/<int:pk>/', views.DetailCustomer.as_view(), name='detail_customer'),
    path('update/customer/<int:pk>/', views.UpdateCustomer.as_view(), name='update_customer'),
    path('delete/customer/<int:pk>/', views.DeleteCustomer.as_view(), name='delete_customer'),

    path('list/ItemPedido', views.list_ItemPedido.as_view(), name="Itempedido"),
    path('recover/ItemPedido/<int:pk>/', views.RecoverItemPedido.as_view(), name='recover_itempedido'),
    path('update/ItemPedido/<int:pk>/', views.UpdateItemPedido.as_view(), name='update_itempedido'),
    path('delete/ItemPedido/<int:pk>/', views.DeteleItemPedido.as_view(), name='delete_itempedido'),  
   
    path('list/Pedido', views.list_pedidos.as_view(), name="pedido"),
    path('detail/Pedido/<int:pk>/', views.Detailpedidos.as_view(), name='detail_pedido'),
    path('update/Pedido/<int:pk>/', views.Updatepedidos.as_view(), name='update_pedido'),
    path('delete/Pedido/<int:pk>/', views.Deletepedidos.as_view(), name='delete_pedido'),   
    
]



