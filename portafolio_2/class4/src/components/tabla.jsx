import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';

const MiDataTable = () => {
  const [simpson, serSimpson] = useState([]);
  const url = 'https://thesimpsonsquoteapi.glitch.me/quotes?count=30'

    const showData= async()=>{
      const response = await fetch(url)
      const data = await response.json()
      serSimpson(data)
    }
    useEffect( ()=>{
      showData()
    },[])

  const columns = [
    
    {
      name: 'Personaje',
      selector: row =>row.character,
    },
    {
      name: 'image',
      selector: row =><img src={row.image} style={{width:50, height:100}}/>,
    },
    {
      name: 'Frase',
      selector: row =>row.quote,
    },
    {
      name: 'posicion',
      selector: row =>row.characterDirection,
    },

  ];

  return (
    <DataTable
    columns={columns}
    data={simpson}
    pagination
    />
      
  );
};

export default MiDataTable;
