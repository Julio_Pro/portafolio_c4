
import './App.css';
import Cabesera from './componetes/cabesera';
import Carrusel from './componetes/carrusel';
import Pestana from './componetes/pestanas.jsx';
import Pie from './componetes/piedepagina.jsx';

const cssbootstrap = require('./static/css/bootstrap.min.css')
const jsbootstrap = require('./static/js/bootstrap.bundle.min.js')

function App() {
  return (
    <div className="App">
      <a href={cssbootstrap} rel="stylesheet" integrity="" crossorigin=""/>
      <script src={jsbootstrap} integrity="" crossorigin=""></script>
      <Cabesera/>
      
      <Carrusel/>
      <br/>
      <br/>

      <Pestana/>
      <br/>

      <Pie/>
    </div>
  );
}

export default App;
