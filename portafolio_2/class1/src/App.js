import './App.css';
// import Headerjs from "./componets/cabesera";
import Welcome from './componets/Welcome';
import Textoo from './componets/texto';
import Imagenes from './componets/imagen';
import Tabla3x3 from './componets/tabla';
import Formulario from './componets/formulario';

const origenes = require('./assets/imagens/4.png')

function App() {
  return (
    <div className="App">
      <Welcome name='food' msg='como estas' saludo='hola'/>
      <Welcome name='food'/>
      <Welcome msg='como estas'/>
      {/* <Headerjs/> */}
      <Textoo tex='un ejemplo'/>
      <p> hola mundo </p>
      <Imagenes lol={origenes} />
      <Tabla3x3/>
      <Formulario/>
    </div>
  );
}

export default App;