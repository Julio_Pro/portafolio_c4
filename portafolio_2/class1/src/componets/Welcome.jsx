export default function Welcome(props){
    return(
        <h1>
            {props.saludo} {props.name} Bienvenido response: {props.msg}
        </h1>
    );
}
