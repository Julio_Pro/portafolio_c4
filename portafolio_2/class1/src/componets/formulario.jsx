import { React, useState } from "react";

export default function Formulario(){
    const {firstname, setfirstName} = useState('');
    const {lastname, setlastName} = useState('');

    const fullname = firstname + ' ' +lastname

    function handfirstname(e){
        setfirstName(e.target.value);
    }
    function handlastname(e){
        setlastName(e.target.value);
    }
    return(
        <>  
            <div class="container-md">
            <div class="form-floating mb-3">
            <h2>Iniciar secion</h2>

            <label class='form-control' htmlFor="firstName">Nombre: </label>
            <input  class="floatingInput" title="text" value={firstname} onChange={handfirstname}/>
            <br/>
            <label class='form-control' htmlFor="lastName">Apellido: </label>
            <input class="floatingInput" title="text" value={lastname} onChange={handlastname}/>
            <br/>
            <br/>
            <button class="btn btn-success">Ola</button>
            </div>
            </div>
        </>
    );
}