import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image } from 'react-native';

import ImageViewer from './components/ImageViewer';
import Button from './components/Button';

const PlaceholderImage = require('./assets/images/Imagen1.png');
export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <ImageViewer PlaceholderImageSource={PlaceholderImage}/>
      </View>
      <View style={styles.footerConteiner}>
        <Button label={'eliji la foto'}/>
        <Button label={'usa esta foto'}/>
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3D425E',
    alignItems: 'center',
    justifyContent: 'center',
  },
  colorText:{
    color:'#fff'
  },
  images:{
    width:320,
    height:440,
    borderRadius: 18,
  },
  imageContainer:{
    flex:1,
    paddingTop:58
  },
  footerConteiner:{
    flex:1/3,
    alignItems: 'center',
  }
});
