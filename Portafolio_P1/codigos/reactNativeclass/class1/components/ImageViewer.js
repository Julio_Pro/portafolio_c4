import { StyleSheet, Image } from "react-native";

export default function ImageViewer({PlaceholderImageSource}){
    return(
        <Image source={PlaceholderImageSource} style={styles.images}/>

    );
}

const styles = StyleSheet.create({
    images:{
      width:320,
      height:440,
      borderRadius: 18,
    }
  });
  