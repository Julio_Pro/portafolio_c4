// Declaración de variable 'message' con valor 'mundo'
// y se imprime en consola 'ola mundo'
// let message = 'mundo';
// console.log('ola', message);

// Declaración de variables 'num' y 'flot'
// y se imprimen en consola
// var num=1
// var flot=1.3
// console.log(num);
// console.log(flot);
// console.log(num +flot);

// Declaración de variables 'admin' y 'name',
// asignación de valor de 'name' a 'admin',
// y se imprime en consola el valor de 'admin'
// var admin 
// var name = 'John';
// admin = name;
// console.log(admin)

// Intento de imprimir variable 'nameplaner' (no definida)
// console.log(nameplaner,' ', userwebb )

// Declaración de objeto 'obj' y se recorre para imprimir cada propiedad
// const obj={
//     name:'julio',
//     age: 20,
//     city:'cansas'
// }
// for(let llave of Object.keys(obj)){
//     console.log(llave, ':', obj[llave])
// }
// console.log('age',obj.age)

// Bucle for-of para iterar sobre un iterable e imprimir cada elemento
// const iterable =[0,1,2]
// for(let i of iterable){
//     console.log(i)
// }

// Bucle for-in para iterar sobre las propiedades de un objeto e imprimir cada una
// for(let key in obj){
//     console.log(key, ':', obj[key])
// }

// Bucles while y do-while para imprimir números hasta cierto límite
// var i = 10
// while (i < 150){
//     console.log(i)
//     i+=5
// }
// i=5
// do{
//     console.log(i)
//     i+=5
// }while(i<100)

// Ejemplo de condicional if-else y operador ternario
// var animal ='kitty'
// if(animal==='kitty'){
//     console.log('yes')
// }else{
//     console.log('no')
// }
// var cat =(animal ==='kitty')? "yes it is" : "ni is not"
// console.log(cat)

// Ejemplo de switch-case para imprimir el resultado según el valor de 'classrom'
// var classrom =1
// switch(classrom){
//     case 1:
//         console.log('clase 1')
//         break;
//     case 2:
//         console.log('clase 2')
//         break;
//     case 3:
//         console.log('clase 3')
//         break;
//     default:
//         console.log('no existe')
// }

// Declaración de función 'off' que contiene una función interna 'on'
// var a = 'mundo'
// function off(){
//     var a = 'ola'
//     console.log(a)
//     function on(){
//       var b ='adios'
//       console.log(a)  
//       console.log(b)
//     }
//     on()
// }
// off()
// console.log(a)

// Declaración de función 'area' para calcular el área de un triángulo
// var base = 89
// var altura =56
// function area(a, b){
//     return (a*b)/2
// }
// var total = area(base,altura)
// console.log('el total es',total)

// Declaración de función anidada 'prism' y llamada encadenada para calcular el volumen
// var l =89;
// var w =23;
// var h =12;
// function prism(l){
//     return function(w){
//         return function(h){
//             return l*w*h
//         }
//     }
// }
// console.log('el resultado es:', prism(l)(w)(h))

// Funciones autoinvocadas (IIFE) que imprimen un mensaje
// (function(){
//     console.log('soy veloz').
// })();

// (function(mensag){
//     console.log('soy veloz '+ mensag)
// })('ola');

// Declaración de funciones con expresión de función y llamadas a las mismas
// const fe=function(){
//     console.log('soy veloz')
// };
// fe()

// const fx = function sum(x,y){
//     return x + y
// }
// const fy = function (g,h){
//     return g+h
// }
// console.log('fx', fx(45, 211), ' fy', fy(65,89))

// Declaración de función recursiva 'di' para imprimir un mensaje varias veces
// var di = function(timpo){
//     if(timpo>0){
//         console.log('hola!')
//         di(timpo-1)
//     }
// }
// var dihola = di;
// di(2)
// console.log('mitad')
// dihola(3)

// Redefinición de la función 'di' como una cadena y posterior invocación
// var di = function di(timpo){
//     if(timpo>0){
//         console.log('hola!')
//         di(timpo-1)
//     }
// }
// var dihola = di;
// di='0la'
// console.log(di)
// dihola(3)

// Declaración de función 'entradasdepersonas' con parámetro restante
// function entradasdepersonas(persona, ...msg){
//     msg.forEach(argumento =>{
//         console.log(persona, 'dice', argumento);
//     });
// }
// entradasdepersonas('juan', 'que ases', 'como estas','adios', 'nooo')

// Uso de parámetro restante y operador spread en una función
// const logargss = (...args)=> console.log(args);
// const lista=[1,2,3]
// logargss('a','b','c', ...lista)

// Declaración de función 'persona_di' con parámetro restante y llamada a la misma
// function persona_di(persona, ...mensaje){
//     mensaje.forEach(argumento => {
//         console.log(persona,'dic', argumento)
//     })
// }
// persona_di('julio', 'ola','mudo','py','script', 'tkiner')

// Función 'funstr' que concatena una cadena y devuelve el resultado
// function funstr(str2){
//     return str2+'hola'
// }
