// Se importa la biblioteca Axios en Node.js
const axios = require('axios')
var url = 'https://jsonplaceholder.typicode.com/posts'

// Se realiza una solicitud GET a la URL proporcionada utilizando Axios.
// Luego se itera sobre los datos recibidos e imprime el título de cada post.
// axios.get(url).then(({data})=>{
//     for(const key in data){
//         console.log(data[key].title)
//     }
// }).catch((err)=>{
//     console.log(err)
//  });


// Se realiza una solicitud POST a la URL proporcionada utilizando Axios.
// Se especifica el cuerpo de la solicitud como un objeto JSON.
// axios.post(url, {
//     userId:2,
//     title:'Lorem'
// }).then(({data})=> console.log(data))

// Se realiza una solicitud GET a la URL proporcionada utilizando Axios.
// Luego se itera sobre los datos recibidos e imprime algunos detalles de cada post.
axios.get(url).then(({data})=>{
    for(const llave in data ){
        console.log('usuario:',data[llave].userId)
        console.log('id:', data[llave].id)
        console.log('titulo:', data[llave].title)
        console.log('cuerpo:', data[llave].body)
        console.log('-----------------------')
    }
})

// Se realiza una solicitud POST a la URL proporcionada utilizando Axios.
// Se especifica el cuerpo de la solicitud como un objeto JSON.
axios.post(url,{
    userId:300,
    id:20,
    title:'œð©',
    body:'descripción'
}).then(({data})=> console.log(data))
