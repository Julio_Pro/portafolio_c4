// Se define una URL de la API de Stack Exchange para obtener preguntas relacionadas con JavaScript.
// Luego se hace una solicitud fetch y se convierte la respuesta a JSON.
// Finalmente, se itera sobre los elementos de las preguntas obtenidas e imprime el título de cada una.
// const url ='http://api.stackexchange.com/2.2/questions?site=stackoverflow&tagged=javascript';

// const responseData = fetch(url).then(response => response.json());

// responseData.then(({items, has_more, quota_max, quota_remaining}) => {
//     for (const {title, score, owner, link, answer_count} of items) {
//         console.log('Question Title --- ',title)
//     }
// })


// Se define una URL de la API JSONPlaceholder para obtener usuarios.
// Se realiza una solicitud fetch y se convierte la respuesta a JSON.
// Luego se itera sobre los usuarios obtenidos e imprime algunos detalles de cada uno.
// var url ='https://jsonplaceholder.typicode.com/users'
// fetch(url).then(response => response.json())
//     .then(response =>{
//       for(var key in response){
//         console.log(
//             'username:', response[key].username, 
//             'email:', response[key].email, 
//             'city:', response[key].address.city,
//             'zipcode:', response[key].address.zipcode, 
//             'company:', response[key].address.zipcode, 
//     )  
//     }
//   })


// Se define una URL de la API JSONPlaceholder para obtener álbumes.
// Se realiza una solicitud fetch y se convierte la respuesta a JSON.
// Luego se itera sobre los álbumes obtenidos e imprime algunos detalles de cada uno.
// var url ='https://jsonplaceholder.typicode.com/albums'
// fetch(url).then(response => response.json())
//     .then(response =>{
//       for(let yo_gane=0; yo_gane!=20; yo_gane++){
//         console.log(
//             'id:', response[yo_gane].id, 
//             'userid:', response[yo_gane].userId, 
//             'title:', response[yo_gane].title,
//       )  
//     }
//   })


// Se realiza una solicitud POST a la URL proporcionada con un objeto JSON en el cuerpo.
// La solicitud utiliza el método POST y especifica los encabezados y el cuerpo de la solicitud.
// fetch(url,{
//   method: 'POST',
//   headers:{'Content_type':'aplication/json'},
//   body: JSON.stringify({userId :101, id:101, title:'juan'})
// }).then(response => response.json())
//     .then(response => console.log(response))


// Se realiza una solicitud POST a la URL proporcionada con un objeto JSON en el cuerpo.
// La solicitud utiliza el método POST y especifica los encabezados y el cuerpo de la solicitud.
// var url2 ='https://jsonplaceholder.typicode.com/users'
// fetch(url2,{
//   method: 'POST',
//   headers:{'Content_type':'aplication/json'},
//   body: JSON.stringify({ /* Objeto JSON con detalles de usuario */ })
// }).then(response => response.json())
//     .then(response => ola(response))
  
// Función que se ejecuta después de recibir la respuesta de la solicitud POST.
// La función recorre las claves del objeto de respuesta y las imprime.
// function ola(obj){
//   for(let llave of Object.keys(obj)){
//     console.log()
// } 
// }
